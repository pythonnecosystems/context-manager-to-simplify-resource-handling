# Python: 리소스 처리를 간소화하는 컨텍스트 관리자

### 들어가며
Python의 컨텍스트 관리자는 파일 처리, 네트워크 연결, 데이터베이스 연결과 같은 리소스를 관리하는 데 사용되는 객체이다. 컨텍스트 관리자는 리소스가 더 이상 필요하지 않을 때 자동으로 리소스를 할당하고 해제하는 방법을 제공하여 적절한 정리를 보장하고 리소스 누수를 방지한다.

컨텍스트 관리자는 일반적으로` with `문과 함께 사용되며, 예외가 발생하더라도 컨텍스트 관리자가 관리하는 리소스가 블록이 끝날 때 적절하게 해제되도록 보장한다.

Python은 컨텍스트 관리자를 만드는 두 가지 주요 방법을 제공한다. `__enter__`와 `__exit__` 메서드가 있는 클래스를 정의하는 방법과 `contextlib` 모듈의 `contextmanager` 데코레이터를 사용하는 방법이다.

## 커스텀 컨텍스트 관리자
아래 예를 생각하여 보자.

```python
f = open('sample. txt', 'w')
f.write('This is test.')
f.close()
```

따라서 이 예에서는 매번 파일을 닫는 것을 기억해야 한다.

따라서 오류가 발생해도 파일을 닫는 컨텍스트 관리자를 사용하는 것이 바람직하다.

```python
with open('sample.txt', 'w') as f:
    f.write('This is test.')
```

따라서 이것은 파일에만 유용할 뿐만 아니라 다음과 같은 용도로도 사용할 수 있다.

- 데이터베이스 연결을 자동으로 연결하고 닫는다,
- 잠금 설정 또는 해제,
- 파일 처리,
- 네트워크 연결 등

또한 사용자 정의 리소스를 처리하기 위해 자체 컨텍스트 관리자를 작성할 수도 있다. 클래스나 함수와 데코레이터를 사용할 수 있다.

### 클래스 사용

```python
class OpenFile:
    def __init__(self, filename, mode) :
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        self.file = open(self.filename, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_val, traceback):
        self.file.close()


with OpenFile('sample.txt', 'w') as f:
    f.write('Testing')
    
print(f.closed)
```

따라서 컨텍스트 관리자 - `with`를 사용하여 클래스를 호출하면 `__init__` 메서드로 이동하여 속성을 설정한다. 그런 다음 `__enter__` 메서드 내에서 파일을 열고 파일 변수를 반환하는 코드를 실행한다. 따라서 **컨텍스트 관리자**의 `f` 변수에 반환 변수를 설정하면 원하는 방식으로 작업할 수 있다. 이제 해당 블록을 종료하면 `self.file.close()`를 호출하는 `__exit__` 메서드가 호출된다. 따라서 `f.closed()`를 인쇄하면 `True`가 반환된다.

### 함수와 데코레이터 사용

```python
from contextlib import contextmanager


@contextmanager
def open_file(file, mode):
    f = open(file, mode)
    yield f
    f.close()


with open_file('sample.txt', 'w') as f:
    f.write('This is test.')

print(f.closed)
```

여기서는 `class`에서 했던 것과 동일한 것을 `open_file` 함수가 도핑하는 것을 볼 수 있다. 여기서는 `yield` 문을 사용하고 있다. yield 문 앞의 모든 것은 클래스의 `__enter__` 메서드와 동일하다. `yield`는 `with` 문 안에 있는 코드가 실행될 때, 즉 `class`의 `__enter__` 메서드에서 파일을 반환하려고 할 때와 동일하다. 그리고 `yield` 문 뒤의 모든 것은 클래스의 `__exit__` 메서드와 동일하다.

클래스 코드에서와 마찬가지로 with 문을 사용하고 generator 함수를 호출하는 것과 동일한 작업을 수행하고 있다.

## 사용 사례

```python
import os


cwd = os.getcwd()   # get current working directory
os.chdir('directory_1')  # changing directory
print(os.listdir())     # listing files
os.chdir(cwd)   # changing directory back to current

cwd = os.getcwd()
os.chdir('directory_2')
print(os.listdir())
os.chdir(cwd)
```

보시다시피 현재 작업 디렉터리를 가져온 다음 다른 디렉터리로 변경하고 몇 가지 작업을 수행 한 다음 다시 현재 디렉토리로 다시 변경하고 있다. 디렉토리를 저장하고, 디렉토리를 변경하고, 다시 전환하는 과정이 조금 불편하다. 그래서 매번 이 작업을 기억해야 한다.

그래서 여기에서 커스텀 컨텍스트 관리자를 사용할 수 있다. 이제 현재 디렉터리를 가져와서 다른 디렉터리로 변경한 다음 각 작업이 완료되면 원래 디렉터리로 다시 전환하는 것에 대해 걱정할 필요가 없다.

```python
import os
from contextlib import contextmanager


@contextmanager
def change_dir(destination):
    try:
        cwd = os.getcwd()
        os.chdir(destination)
        yield
    finally:
        os.chdir(cwd)

with change_dir('directory_1'):
    print(os.listdir())
with change_dir('directory_2'):
    print(os.listdir())
```

## 끝내며
컨텍스트 관리자는 리소스를 설정하고 해체할 때 정말 유용하다. 오류가 발생하면 컨텍스트 관리자 메커니즘이 자동으로 리소스를 정리해 준다.

컨텍스트 관리자는 리소스의 획득과 해제를 자동으로 처리하여 코드를 가독성을 높이며 더욱 간결하고 견고하게 만들어 준다.

특히 버그와 성능 문제를 피하기 위해 적절한 리소스 관리가 중요한 시나리오에서 유용하다.
